import logging
import os

from flask import Flask, make_response, render_template

from apps.globals import VERSION
from apps.root import output

app = Flask(__name__)

FMT = "[%(asctime)s] %(levelname)s [%(filename)s:%(lineno)d] [%(funcName)s] %(message)s"
LOGLEVEL = logging.INFO if os.environ.get("RUNMODE") == "production" else logging.DEBUG
logging.basicConfig(format=FMT, level=LOGLEVEL)

# ************************************************************************
# **************************** SERVICE ROUTES ****************************
# ************************************************************************


@app.route("/query", methods=["GET"])
def query():
    return output("resp")


@app.route("/application.wadl")
def wadl():
    template = render_template("resp_wadl.xml")
    response = make_response(template)
    response.headers["Content-Type"] = "application/xml"
    return response


@app.route("/version", strict_slashes=False)
def version():
    response = make_response(VERSION)
    response.headers["Content-Type"] = "text/plain"
    return response


@app.route("/commit", strict_slashes=False)
def commit():
    try:
        with open("./static/commit.txt") as commit_file:
            COMMIT_SHORT_SHA = commit_file.readline()
    except Exception:
        COMMIT_SHORT_SHA = "unspecified"
    response = make_response(COMMIT_SHORT_SHA)
    response.headers["Content-Type"] = "text/plain"
    return response


@app.route("/")
@app.route("/local=fr")
def doc():
    return render_template("resp_doc.html")


@app.route("/local=en")
def doc_en():
    return render_template("resp_doc_en.html")


# **** MAIN ****
if __name__ == "__main__":
    app.run()
