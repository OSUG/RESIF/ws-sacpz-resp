# Webservice SACPZ and RESP

## Go on with Flask

    FLASK_ENV=development RUNMODE=test FLASK_APP=sacpz python start_sacpz.py

or

    FLASK_ENV=development RUNMODE=test FLASK_APP=sacpz python start_resp.py

## Play around with docker

```
docker build -t ws-sacpz-resp .
docker run --rm -e RUNMODE=test -p 8000:8000 --name ws-sacpz-resp ws-sacpz-resp:latest
```

Then :

```
wget -O - http://localhost:8000/1/application.wadl
```

Run it in debug mode with flask :

```
docker build -t ws-sacpz-resp .
docker run --rm --name ws-sacpz -e RUNMODE=production ws-sacpz-resp
# WS-RESP
docker run --rm --name ws-resp  -e RUNMODE=production ws-sacpz-resp start_resp:app
```

## RUNMODE builtin values

  * `production`
  * `test`
  * `local`
  * other values map to :
