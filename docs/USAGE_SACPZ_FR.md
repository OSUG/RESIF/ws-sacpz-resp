# Webservice sacpz

Ce service donne les pôles et les zéros des réponses instrumentales sélectionnées au format ASCII SACPZ.

## Utilisation de la requête

    /query? (channel-options) [time-option] [nodata=404]

    où :

    channel-options      ::  (net=<network>) (sta=<station>) (loc=<location>) (cha=<channel>)
    time-option          ::  [time=<date>] [starttime=<date>] [endtime=<date>]

    (..) requis
    [..] optionnel

## Exemples de requêtes

<a href="http://ws.resif.fr/resifws/sacpz/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&time=2017-01-01T00:00:00">http://ws.resif.fr/resifws/sacpz/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&time=2017-01-01T00:00:00</a>

<a href="http://ws.resif.fr/resifws/sacpz/1/query?net=FR&sta=RUSF&loc=06&cha=??N&time=2017-01-01T00:00:00&nodata=404">http://ws.resif.fr/resifws/sacpz/1/query?net=FR&sta=RUSF&loc=06&cha=??N&time=2017-01-01T00:00:00&nodata=404</a>

## Descriptions détaillées de chaque paramètre de la requête

| Paramètre  | Exemple | Discussion                                                                    |
| :--------- | :------ | :---------------------------------------------------------------------------- |
| net[work]  | FR      | Nom du réseau sismique.                                                       |
| sta[tion]  | CIEL    | Nom de la station.                                                            |
| loc[ation] | 00      | Code de localisation. Utilisez loc=-- pour les codes de localisations vides. Accepte les jokers et les listes. |
| cha[nnel]  | HHZ     | Code de canal. Accepte les jokers et les listes.                              |
| time       | 2017-01-01T00:00:00 | La réponse est évaluée à l'instant donné. Sans précision, le temps actuel est utilisé. Ne peut pas être utilisé avec les paramètres starttime ou endtime |
| start[time] | 2010-01-10T00:00:00 | Donne les réponses qui couvrent la période après la date donnée incluse. |
| end[time]  | 2011-02-11T01:00:00 | Donne les réponses qui couvrent la période avant la date donnée incluse. |

## Formats des dates et des heures

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (une heure de 00:00:00 est supposée)

    avec :

    YYYY    :: quatre chiffres de l'année
    MM      :: deux chiffres du mois (01=Janvier, etc.)
    DD      :: deux chiffres du jour du mois (01 à 31)
    T       :: séparateur date-heure
    hh      :: deux chiffres de l'heure (00 à 23)
    mm      :: deux chiffres des minutes (00 à 59)
    ss      :: deux chiffres des secondes (00 à 59)
    ssssss  :: un à six chiffres des microsecondes en base décimale (0 à 999999)

