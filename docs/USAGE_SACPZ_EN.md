# Webservice sacpz

The RESIF SACPZ web service returns the poles and zeros of the selected instrument responses in SAC ASCII format.

## Query usage

    /query? (channel-options) [time-option] [nodata=404]

    where:

    channel-options      ::  (net=<network>) (sta=<station>) (loc=<location>) (cha=<channel>)
    time-option          ::  [time=<date>] [starttime=<date>] [endtime=<date>]

    (..) required
    [..] optional

## Sample queries

<a href="http://ws.resif.fr/resifws/sacpz/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&time=2017-01-01T00:00:00">http://ws.resif.fr/resifws/sacpz/1/query?net=FR&sta=CIEL&loc=00&cha=HHE&time=2017-01-01T00:00:00</a>

<a href="http://ws.resif.fr/resifws/sacpz/1/query?net=FR&sta=RUSF&loc=06&cha=??N&time=2017-01-01T00:00:00&nodata=404">http://ws.resif.fr/resifws/sacpz/1/query?net=FR&sta=RUSF&loc=06&cha=??N&time=2017-01-01T00:00:00&nodata=404</a>

## Detailed descriptions of each query parameter

| Parameter  | Example  | Discussion                                                                       |
| :--------- | :------- | :--------------------------------------------------------------------------------|
| net[work]  | FR       | Seismic network name.                                                            |
| sta[tion]  | CIEL     | Station name.                                                                    |
| loc[ation] | 00       | Location code. Use loc=-- for empty location codes. Accepts wildcards and lists. |
| cha[nnel]  | HHZ      | Channel Code. Accepts wildcards and lists.                                       |
| time       | 2017-01-01T00:00:00 | Evaluate the response at the given time. If not specified, the current time is used. Cannot be used with starttime or endtime parameters |
| start[time] | 2010-01-10T00:00:00 | Specify the responses that cover the time period after (and including) this time. |
| end[time]  | 2011-02-11T01:00:00 | Specify responses that cover the time period prior to this time. |

## Date and time formats

    YYYY-MM-DDThh:mm:ss[.ssssss] ex. 1997-01-31T12:04:32.123
    YYYY-MM-DD ex. 1997-01-31 (a time of 00:00:00 is assumed)

    where:

    YYYY    :: four-digit year
    MM      :: two-digit month (01=January, etc.)
    DD      :: two-digit day (01 through 31)
    T       :: date-time separator
    hh      :: two-digit hour (00 through 23)
    mm      :: two-digit number of minutes (00 through 59)
    ss      :: two-digit number of seconds (00 through 59)
    ssssss  :: one to six-digit number of microseconds (0 through 999999)

