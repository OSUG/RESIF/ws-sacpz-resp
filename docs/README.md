### The HTML documentation located in "templates/" directory is generated as follows:

1) Modify your markdown files: USAGE_*.md
2) Run md2html.sh script:

    bash md2html.sh "markdown file name" "html file name" "html page title"


### Examples:

    bash md2html.sh USAGE_RESP_FR.md resp_doc.html "RESIF: RESIFWS: Resp Docs: v1"
    bash md2html.sh USAGE_RESP_EN.md resp_doc_en.html "RESIF: RESIFWS: Resp Docs: v1"

    bash md2html.sh USAGE_SACPZ_FR.md sacpz_doc.html "RESIF: RESIFWS: Sacpz Docs: v1"
    bash md2html.sh USAGE_SACPZ_EN.md sacpz_doc_en.html "RESIF: RESIFWS: Sacpz Docs: v1"


### Requirements:

pandoc
