# Building heavy python dependencies as a first stage
# will build ospy for instance
FROM python:3.9-slim AS python-deps
RUN apt-get update && apt-get install -y --no-install-recommends gcc libc6-dev
COPY requirements.txt /
RUN pip install --no-cache-dir -r /requirements.txt


FROM python:3.9-slim
MAINTAINER RESIF DC <resif-dc@univ-grenoble-alpes.fr>
# Get the dependencies from previous stage
COPY --from=python-deps /usr/local/lib/python3.9/site-packages /usr/local/lib/python3.9/site-packages
RUN pip install --no-cache-dir gunicorn
RUN mkdir -p /usr/share/man/man1
RUN apt-get update && apt-get install -y wget openjdk-11-jre-headless && apt-get clean
WORKDIR /appli
RUN wget -O stationxml-seed-converter-2.1.0.jar https://github.com/iris-edu/stationxml-seed-converter/releases/download/stationxml-seed-converter-2.1.0/stationxml-seed-converter-2.1.0.jar
COPY start_resp.py start_sacpz.py ./
COPY apps ./apps/
COPY templates ./templates/
COPY static ./static/
USER 1000

ENTRYPOINT ["gunicorn",  "--bind",  "0.0.0.0:8000"]
CMD ["start_sacpz:app"]
