import glob
import logging
import time
import re
from io import StringIO
from subprocess import Popen
from tempfile import TemporaryDirectory

from flask import make_response
from obspy.clients.fdsn import Client
from obspy.io.xseed.parser import Parser

from apps.globals import Error
from apps.globals import FDSN_CLIENT
from apps.globals import STATIONXML_CONVERTER
from apps.globals import USER_AGENT_RESP
from apps.globals import USER_AGENT_SACPZ
from apps.utils import error_request
from apps.utils import tictac


def get_inventory(params, user_agent):
    client = Client(FDSN_CLIENT, user_agent=user_agent)
    start = params["time"] if params["time"] is not None else params["starttime"]
    end = params["time"] if params["time"] is not None else params["endtime"]
    return client.get_stations(
        network=params["network"],
        station=params["station"],
        location=params["location"],
        channel=params["channel"],
        starttime=start,
        endtime=end,
        level="response",
    )


def get_sacpz(params):
    """Builds the ws-sacpz response.
    params: parameters"""

    try:
        tic = time.time()
        response = None

        cid = f"{params['network']}.{params['station']}.{params['location']}.{params['channel']}"
        start = (
            params["starttime"] if params["starttime"] is not None else params["time"]
        )
        fcid = cid + "." + str(start).replace(" ", "T")
        logging.debug(fcid)

        try:
            inventory = get_inventory(params, USER_AGENT_SACPZ)
        except Exception as ex:
            logging.debug(str(ex))
            code = int(params["nodata"])
            return error_request(msg=f"HTTP._{code}_", details=Error.NODATA, code=code)

        data = StringIO()
        inventory.write(data, format="SACPZ")
        headers = {"Content-type": "text/plain ; charset=utf-8"}
        response = make_response(data.getvalue(), headers)
        return response
    except Exception as ex:
        logging.exception(str(ex))
    finally:
        if response:
            nbytes = response.headers.get("Content-Length")
            logging.info(f"{nbytes} bytes rendered in {tictac(tic)} seconds.")


def get_resp(params):
    """Builds the ws-resp response.
    params: parameters"""

    try:
        tic = time.time()
        response = None

        cid = f"{params['network']}.{params['station']}.{params['location']}.{params['channel']}"
        start = (
            params["starttime"] if params["starttime"] is not None else params["time"]
        )
        fcid = cid + "." + str(start).replace(" ", "T")
        logging.debug(fcid)

        try:
            inventory = get_inventory(params, USER_AGENT_RESP)
        except Exception as ex:
            logging.debug(str(ex))
            code = int(params["nodata"])
            return error_request(msg=f"HTTP._{code}_", details=Error.NODATA, code=code)

        with TemporaryDirectory() as tmp:
            # save inventory in the stationxml file format
            stationxml = tmp + "/" + "stationxml"
            inventory.write(stationxml, format="STATIONXML")

            # convert stationxml in dataless
            dataless = tmp + "/" + "dataless"
            cmd = f"{STATIONXML_CONVERTER} {stationxml} --output {dataless}"
            with Popen(cmd, shell=True, executable="/bin/bash") as proc:
                logging.error(proc.stderr)

            # convert dataless in RESP
            Parser(dataless).write_resp(folder=tmp, zipped=False)

            # append RESP files in a string buffer
            data = StringIO()
            for filename in glob.glob(f"{tmp}/RESP.*"):
                with open(filename) as myfile:
                    for line in myfile.readlines():
                        # Si la date n'a pas d'information de temps, alors on doit ajouter des 0
                        newline=re.sub(r"( date: +[0-9]{4}.[012][0-9]{2})\n", r"\1,10:00:00.0000\n", line)
                        data.write(newline)

            data = data.getvalue()
            if not data:
                logging.error("No RESP files or files empty!")
                raise Exception

        headers = {"Content-type": "text/plain ; charset=utf-8"}
        response = make_response(data, headers)
        return response
    except Exception as ex:
        logging.exception(str(ex))
    finally:
        if response:
            nbytes = response.headers.get("Content-Length")
            logging.info(f"{nbytes} bytes rendered in {tictac(tic)} seconds.")
