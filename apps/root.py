import logging
import queue
import re
from multiprocessing import Process, Queue

from flask import request

from apps.globals import Error
from apps.globals import HTTP
from apps.globals import TIMEOUT
from apps.output import get_resp
from apps.output import get_sacpz
from apps.parameters import Parameters
from apps.utils import check_base_parameters
from apps.utils import check_request
from apps.utils import currentutcday
from apps.utils import error_param
from apps.utils import error_request
from apps.utils import is_valid_datetime
from apps.utils import is_valid_nodata
from apps.utils import is_valid_starttime


def check_parameters(params):

    # check base parameters
    (params, error) = check_base_parameters(params)
    if error["code"] != 200:
        return (params, error)

    # is both time and starttime (or endtime) are defined ?
    if params["time"] is not None and params["start"] is not None:
        return error_param(params, Error.MULTI_TIME + " starttime parameter.")

    if params["time"] is not None and params["end"] is not None:
        return error_param(params, Error.MULTI_TIME + " endtime parameter.")

    # time validations
    if params["time"] is not None:
        if not is_valid_starttime(params["time"]):
            return error_param(params, Error.TIME + str(params["time"]))

        if is_valid_datetime(params["time"]):
            params["time"] = is_valid_datetime(params["time"])
    elif params["start"] is None and params["end"] is None:
        params["time"] = currentutcday()

    # nodata parameter validation
    if not is_valid_nodata(params["nodata"]):
        return error_param(params, Error.NODATA_CODE + str(params["nodata"]))
    params["nodata"] = params["nodata"].lower()

    # wildcards or list are allowed only with location and channel parameters
    for key in ("network", "station"):
        if re.search(r"[,*?]", params[key]):
            return error_param(params, Error.NO_WILDCARDS + key)

    for key, val in params.items():
        logging.debug(key + ": " + str(val))

    return (params, {"msg": HTTP._200_, "details": Error.VALID_PARAM, "code": 200})


def checks_get():

    # get default parameters
    params = Parameters().todict()

    # check if the parameters are unknown
    (p, result) = check_request(params)
    if result["code"] != 200:
        return (p, result)
    return check_parameters(params)


def output(webservice):

    try:
        process = None
        result = {"msg": HTTP._400_, "details": Error.UNKNOWN_PARAM, "code": 400}
        logging.debug(request.url)

        (params, result) = checks_get()
        if result["code"] == 200:

            def put_response(q):
                if webservice == "resp":
                    q.put(get_resp(params))
                elif webservice == "sacpz":
                    q.put(get_sacpz(params))

            q = Queue()
            process = Process(target=put_response, args=(q,))
            process.start()
            resp = q.get(timeout=TIMEOUT)

            if resp:
                return resp
            else:
                raise Exception

    except queue.Empty:
        result = {"msg": HTTP._408_, "details": Error.TIMEOUT, "code": 408}

    except Exception as excep:
        result = {"msg": HTTP._500_, "details": Error.UNSPECIFIED, "code": 500}
        logging.exception(str(excep))

    finally:
        if process:
            process.terminate()

    return error_request(
        msg=result["msg"], details=result["details"], code=result["code"]
    )
