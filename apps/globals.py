FDSN_CLIENT = "RESIF"
USER_AGENT_RESP = "resifws-resp"
USER_AGENT_SACPZ = "resifws-sacpz"
STATIONXML_CONVERTER = "java -jar stationxml-seed-converter-2.1.0.jar"
VERSION = "1.0.0"


# limitations
TIMEOUT = 120

# available parameter values
NODATA_CODE = ("204", "404")
STRING_TRUE = ("yes", "true", "t", "y", "1", "")
STRING_FALSE = ("no", "false", "f", "n", "0")


class Error:
    UNKNOWN_PARAM = "Unknown query parameter: "
    MULTI_PARAM = "Multiple entries for query parameter: "
    VALID_PARAM = "Valid parameters."
    START_LATER = "The starttime cannot be later than the endtime: "
    TOO_LONG_DURATION = "Too many days requested (greater than "
    UNSPECIFIED = "Error processing your request."
    NODATA = "Your query doesn't match any available data."
    TIMEOUT = f"Your query exceeds timeout ({TIMEOUT} seconds)."
    MISSING = "Missing parameter: "
    BAD_VAL = " Invalid value: "
    CHAR = "White space(s) or invalid string. Invalid value for: "
    EMPTY = "Empty string. Invalid value for: "
    BOOL = "(Valid boolean values are: true/false, yes/no, t/f or 1/0)"
    NETWORK = "Invalid network code: "
    STATION = "Invalid station code: "
    LOCATION = "Invalid location code: "
    CHANNEL = "Invalid channel code: "
    TIME = "Bad date value: "
    INT_BETWEEN = "must be an integer between"
    PROCESSING = "Your request cannot be processed. Check for value consistency."
    NODATA_CODE = f"Accepted nodata values are: {NODATA_CODE}." + BAD_VAL
    NO_SELECTION = "Request contains no selections."
    NO_WILDCARDS = (
        "Wildcards or list are allowed only with location and channel parameters."
        + BAD_VAL
    )
    MULTI_TIME = "The time parameter cannot be used in combination with"


class HTTP:
    _200_ = "Successful request. "
    _204_ = "No data matches the selection. "
    _400_ = "Bad request due to improper value. "
    _401_ = "Authentication is required. "
    _403_ = "Forbidden access. "
    _404_ = "No data matches the selection. "
    _408_ = "Request exceeds timeout. "
    _413_ = "Request too large. "
    _414_ = "Request URI too large. "
    _500_ = "Internal server error. "
    _503_ = "Service unavailable. "
